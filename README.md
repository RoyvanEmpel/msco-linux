# My Summer Car Online on linux

### Prerequisites
- [Wine](https://www.winehq.org/) (wine --version)
  - Tested: wine-8.21 (Staging)
- [Winetricks](https://wiki.winehq.org/Winetricks)
  - Tested: 20220411 (winetricks --version)
- Proton Installed on steam
  - Tested: Proton 8.0
- [My summer car](https://store.steampowered.com/app/516750/My_Summer_Car/) installed on steam
- My summer car has been run at least once
- My summer car doesn't have any mods or mod loaders installed
- Have the `libavfilter8 libvpx7` packages installed on your system.
  (apt install for debian based distro)

---

### Installation

#### Step 1 - Download
Download the most recent version of MSCO from the website. [Download](https://www.mysummercar.online/downloadmsco/)

You will need a program to open the rar and unrar it.

#### Step 2 - Prefix
Find your MSC wine prefix, this is usually located here `~/.steam/steam/steamapps/compatdata/516750/pfx`. If you have it installed on a different drive, it may be something like this `/mnt/games-drive/SteamLibrary/steamapps/compatdata/516750/pfx`.

#### Step 3 - Install winetricks
Using the prefix from step 2, run the following command to install dotnet48. This is required for MSCO to run.  {YOUR PREFIX} should be replaced with the path to your prefix (don't forget the /pfx at the end).
```
WINEPREFIX="{YOUR PREFIX}" winetricks dotnet48
```

There may be a crash once you try logging in to MSCO, to fix this install corefonts.
```
WINEPREFIX="{YOUR PREFIX}" winetricks corefonts
```

#### Step 4 - Install MSCO
With the same WINEPREFIX, run the MSCO installer. {PATH TO MSCO INSTALLER} should be replaced with the path to the installer you downloaded in step 1. Just follow the steps in the installer and you should be good to go. (don't forget the /pfx at the end).
**If you get an error click "ignore"**

```
WINEPREFIX="{YOUR PREFIX}" wine "{PATH TO MSCO INSTALLER}"
```



#### Step 5 - Run MSCO
To run MSCO you will first need to find your proton installation, this is usually located here `~/.steam/steam/steamapps/common/Proton 8.0/proton`. If you have it installed on a different drive, it may be something like this `/mnt/games-drive/SteamLibrary/steamapps/common/Proton 8.0/proton`.

For running the command you need the following: (this is not a command)
- STEAM_COMPAT_DATA_PATH={YOUR PREFIX} (WITHOUT /pfx **!! Important !!**)
- STEAM_COMPAT_CLIENT_INSTALL_PATH={STEAM INSTALL PATH} ("~/.steam" by default)


Run the launcher using the following command (enters must be removed):
```
STEAM_COMPAT_DATA_PATH="{YOUR PREFIX - no /pfx}"
STEAM_COMPAT_CLIENT_INSTALL_PATH="{YOUR STEAM PATH}"
"{PATH TO PROTON}/proton" run
"{YOUR PREFIX}/drive_c/Program Files/MSCO Launcher/MSCO Launcher.exe"
```

Filled in example (No enters):
```
STEAM_COMPAT_DATA_PATH="/mnt/game-drive/SteamLibrary/steamapps/compatdata/516750" STEAM_COMPAT_CLIENT_INSTALL_PATH="~/.steam" "/mnt/game-drive/SteamLibrary/steamapps/common/Proton 8.0/proton" run "$STEAM_COMPAT_DATA_PATH/pfx/drive_c/Program Files/MSCO Launcher/MSCO Launcher.exe"
```

#### Step 6 - Add MSCO to steam (optional)

##### Step 6.1
Click on "Add a Game" in the bottom left corner of steam and then click on "Add a Non-Steam Game".

![Add a Non-Steam Game](https://i.imgur.com/z9yOVVL.png)

##### Step 6.2
Search for MSCO, check it and click on "Add Selected Programs". If MSCO isn't showing up, click on "Browse..." and navigate to the MSCO launcher `"{YOUR PREFIX}/drive_c/Program Files/MSCO Launcher/MSCO Launcher.exe"`.

![Search MSCO](https://i.imgur.com/39vW3n8.png)

##### Step 6.3
Right click on MSCO in your library and click on "Properties".

![Properties](https://i.imgur.com/v4iVOs0.png)

##### Step 6.4
Replace the Launch Options with the command from step 5.
![Launch Options](https://i.imgur.com/joKIPuy.png)

---

# Party time! 🎉
![Party time](https://i.imgur.com/I3jfjdz.jpg)
